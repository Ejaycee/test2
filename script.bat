@echo off
setlocal enabledelayedexpansion

rem Array of possible outcomes
set outcomes[0]=success
set outcomes[1]=error1
set outcomes[2]=error2
set outcomes[3]=error3

rem Get the random index
set /a randomIndex=%RANDOM% %% 4

rem Select the outcome
set selected_outcome=!outcomes[%randomIndex%]!

echo Selected outcome: %selected_outcome%

rem Handle the selected outcome
if "%selected_outcome%"=="success" (
    echo Script executed successfully!
    exit /b 0
) else if "%selected_outcome%"=="error1" (
    echo Error: Something went wrong in step 1
    exit /b 1
) else if "%selected_outcome%"=="error2" (
    echo Error: Failed to connect to the database
    exit /b 2
) else if "%selected_outcome%"=="error3" (
    echo Error: Invalid input provided
    exit /b 3
) else (
    echo Unknown outcome
    exit /b 99
)
