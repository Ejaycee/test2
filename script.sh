#!/bin/bash

# Array of possible outcomes
outcomes=("success" "error1" "error2" "error3")

# Randomly select an outcome
selected_outcome=${outcomes[$RANDOM % ${#outcomes[@]}]}

echo "Selected outcome: $selected_outcome"

case $selected_outcome in
  "success")
    echo "Script executed successfully!"
    exit 0  # Success
    ;;
  "error1")
    echo "Error: Something went wrong in step 1"
    exit 1  # Error 1
    ;;
  "error2")
    echo "Error: Failed to connect to the database"
    exit 2  # Error 2
    ;;
  "error3")
    echo "Error: Invalid input provided"
    exit 3  # Error 3
    ;;
  *)
    echo "Unknown outcome"
    exit 99  # Fallback error
    ;;
esac
