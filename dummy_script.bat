@echo off

:simulate_failure
echo Simulating failure...
echo.  **Add a new line here**
exit /b 1

:simulate_success
echo Simulating success...
exit /b 0

if "%1" == "fail" (
  call :simulate_failure
) else (
  call :simulate_success
)

