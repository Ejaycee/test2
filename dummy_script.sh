#!/bin/sh

simulate_failure() {
    echo "Simulating failure..."
    return 1
}

simulate_success() {
    echo "Simulating success..."
    return 0
}

if [ "$1" = "fail" ]; then
    simulate_failure
else
    simulate_success
fi
