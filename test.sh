#!/bin/sh

#set -o pipefail  # Makes pipeline exit on non-zero exit code from any command

simulate_failure() {
  # Simulate an error scenario (replace with your actual error)
  echo "An error occurred!"
  exit 2
}

simulate_success() {
  echo "Simulating success..."
  exit 0
}

if [ "$1" = "fail" ]; then
  simulate_failure
else
  simulate_success
fi
