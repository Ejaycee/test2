#!/bin/sh

which sshpass > /dev/null
if [ $? -ne 0 ]; then
  echo "You need to install sshpass"
  exit 1
fi

username=""
password=""
branch_name="unknown"
hash=""
key=""
servers=""
is_demo=false

usage() {
	echo "Usage: $0 --username <username> --password <password> --hash <hash> --key <key> --servers <comma delimited list of servers> [--branch_name <branch name>]"
}

validate_arg() {
    if [ -z "$2" ] || [ "${2#-}" != "$2" ]; then
        echo "Error: $1 requires a value"
        exit 1
    fi
}

KNOWN_QA="01qa.nonfatmedia.net 02qa.nonfatmedia.net 03qa.nonfatmedia.net 04qa.nonfatmedia.net 05qa.nonfatmedia.net 06qa.nonfatmedia.net 07qa.nonfatmedia.net 08qa.nonfatmedia.net 09qa.nonfatmedia.net 10qa.nonfatmedia.net 11qa.nonfatmedia.net 12qa.nonfatmedia.net 01taskqa.nonfatmedia.net"
KNOWN_STAGING="01staging.nonfatmedia.net 02staging.nonfatmedia.net 03staging.nonfatmedia.net 04staging.nonfatmedia.net 05staging.nonfatmedia.net 06staging.nonfatmedia.net 07staging.nonfatmedia.net 08staging.nonfatmedia.net 09staging.nonfatmedia.net 10staging.nonfatmedia.net 01taskstaging.nonfatmedia.net"

validate_servers() {
    local server

    for server in $servers; do
        local found=0
        for known in $KNOWN_QA; do
            if [ "$server" = "$known" ]; then
                found=1
                break
            fi
        done
        if [ $found -eq 0 ]; then
	        for known in $KNOWN_STAGING; do
	            if [ "$server" = "$known" ]; then
	                found=1
	                break
	            fi
	        done
	    fi
        if [ $found -eq 0 ]; then
            echo "Error: Server '$server' is not recognized."
            exit 1
        fi
    done
}

while [ "$#" -gt 0 ]; do
    case "$1" in
        --username)
            validate_arg "$1" "$2"
            username="$2"
            shift 2
            ;;
        --password)
            validate_arg "$1" "$2"
            password="$2"
            shift 2
            ;;
        --branch_name)
            validate_arg "$1" "$2"
            branch_name="$2"
            is_demo=$( [[ "${branch_name}" == Demos/* ]] && echo "true" || echo "false" )
            shift 2
            ;;
        --hash)
            validate_arg "$1" "$2"
            hash="$2"
            shift 2
            ;;
        --key)
            validate_arg "$1" "$2"
            key="$2"
            shift 2
            ;;
        --servers)
            validate_arg "$1" "$2"
            servers="$(echo "$2" | tr ',' ' ')"
            validate_servers "$servers"
            shift 2
            ;;
        *)
            echo "Invalid argument: $1"
            usage
            exit 1
            ;;
    esac
done

if [ -z "$username" ] || [ -z "$password" ] || [ -z "$hash" ] || [ -z "$key" ] || [ -z "$servers" ]; then
    echo "All parameters except branch_name are required."
    usage
    exit 1
fi

GIT_WORKING_DIR="C:\Sites\www\breakdown-services-full"
LUCEE_STATUS_FILE="${GIT_WORKING_DIR}\_wwwroot\F5status.txt"
updateF5() {
	local server
	local message="${1}"
	for server in $servers; do
		sshpass -p ${password} ssh ${username}@${server} "cmd /c echo|set /p status=\"${message}\"; > ${LUCEE_STATUS_FILE}"
	done
}

removeFromF5() {
	echo "Removing from F5"
	updateF5 "Server unavalable"
}

addToF5() {
	echo "Adding to F5"
	updateF5 "Lucee is responsive"
}

updateCode() {
	echo "Updating code to ${hash}"
	local server
	for server in $servers; do
		sshpass -p ${password} ssh ${username}@${server} "cd ${GIT_WORKING_DIR} && git fetch -p && git checkout -f ${hash} && echo ${branch_name} > global\onbranch.txt"
		if [ $? -ne 0 ]; then
			echo "Checkout of hash ${hash} failed on ${server}"
			exit 1
		fi
	done
}

verifyMasterMerge() {
	echo "Verifying master merge"
	if [ "${is_demo}" = "false" ]; then
		local server
		for server in $servers; do
			sshpass -p ${password} ssh ${username}@${server} "cd ${GIT_WORKING_DIR} && git merge-base --is-ancestor origin/master HEAD"
			if [ $? -ne 0 ]; then
				echo "Master is not merged into ${branch_name} (${hash})"
				exit 1
			fi
		done
	fi
}

updateAppVersion() {
	APPLICATION_VERSION_FILE="${GIT_WORKING_DIR}\global\version.cfm"
	now=$(date +%s)
	echo "Updating app version to ${now}-${branch_name}"
	local server
	for server in $servers; do
		sshpass -p ${password} ssh ${username}@${server} "echo ^<cfset app_version = \"${now}-${branch_name}\" /^> > ${APPLICATION_VERSION_FILE}"
	done
}

restartLucee() {
	echo "Restarting Lucee"
	SCRIPT_DIRECTORY="C:\Sites\Deploy"
	local server
	for server in $servers; do
		sshpass -p ${password} ssh ${username}@${server} "cd ${SCRIPT_DIRECTORY} && lucee_restart.bat && subtasks\createsymlinks.bat"
	done
}

SITES="-casting.breakdownexpress.com -cp.breakdownexpress.com -cp.breakdownservices.com -enterprise.breakdownexpress.com -grid.breakdownservices.com -resumes.actorsaccess.com -resumes.breakdownexpress.com -talentrep.breakdownexpress.com -webservices.breakdownexpress.com .actorsaccess.com .actorsacces.com/api/ios/v2 .auditionservices.com .breakdownexpress.com .breakdownservices.com"
wakeServer() {
	echo "Waking sites"
	local server
	for server in $servers; do
		ip=$(dig +short ${server})
		tier=qa
		if [ "${server%staging.nonfatmedia.net}" != "${server}" ]; then
			tier=staging
		fi
		for site in $SITES; do
			echo "Waking ${tier}${site}"
			curl -s "https://${tier}${site}?nfmnode=${ip}" > /dev/null
		done
	done
}
start_time=$SECONDS

removeFromF5
updateCode
verifyMasterMerge
updateAppVersion
restartLucee
wakeServer
addToF5

elapsed_time=$(($SECONDS - start_time))
echo "Elapsed time: $elapsed_time seconds."